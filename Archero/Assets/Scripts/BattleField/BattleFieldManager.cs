﻿using Core;
using Data;
using UnityEngine;

namespace BattleField
{
    public class BattleFieldManager
    {
        private Vector3 center;
        public Vector3 Center => center;

        private float width;
        public float Width => width;
        

        private float height;
        public float Height => height;

        public void CreateBattleField(BattleFieldInfo info)
        {
            center = new Vector3(0f, 0f, info.height * 0.5f);
            width = info.width;
            height = info.height;
            
            //TODO test
            var obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            obj.transform.localScale = new Vector3(info.width, 0.001f, info.height);
            obj.transform.position = center;

            var renderer = obj.GetComponent<Renderer>();
            renderer.material = info.material;

            foreach (var obstacle in info.obstacles)
            {
                var position = center + obstacle.position;
                Object.Instantiate(obstacle.prefab, position, Quaternion.identity);
            }
        }

        public void SetupCoin(Vector3 position)
        {
            var coinPrefab = Game.Instance.Data.GetCoinPrefab();
            Object.Instantiate(coinPrefab, position, Quaternion.identity);
        }

    }
}
