﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private float referenceSize = 8.3f;
    [SerializeField] private float minSize = 5f;
    [SerializeField] private float referenceAspect = 0.56f;
    [SerializeField] private float minAspect = 0.4f;

    private void Reset()
    {
        mainCamera = GetComponent<Camera>();
    }

    private void Start()
    {
        var aspect = (float) Screen.width / Screen.height;
        if (aspect >= referenceAspect)
        {
            return;
        }
        
        var ratio = (referenceAspect - aspect) / (referenceAspect - minAspect);
        ratio = Mathf.Clamp01(ratio);

        mainCamera.orthographicSize = Mathf.Lerp(referenceSize, minSize, ratio);
    }
}
