using UnityEngine;

namespace Data
{
    [System.Serializable]
    public class EnemyInfo
    {
        public string name;
        public int health = 5;
        public float speed = 2f;
        public int meleeDamage = 1;
        public GameObject prefab;
        public AiData ai;
    }

    [CreateAssetMenu(fileName = "EnemyData", menuName = "Data/EnemyData", order = 0)]
    public class EnemyData : ScriptableObject
    {
        public EnemyInfo[] enemyInfo;
    }
}