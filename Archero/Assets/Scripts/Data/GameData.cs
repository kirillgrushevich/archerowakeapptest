using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "GameData", menuName = "Data/GameData", order = 0)]
    public class GameData : ScriptableObject
    {
        public GameObject coinPrefab;
    }
}