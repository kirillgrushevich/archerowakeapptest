using UnityEngine;

namespace Data
{
    [System.Serializable]
    public class ObstacleInfo
    {
        public GameObject prefab;
        public Vector3 position;
    }

    [System.Serializable]
    public class EnemyOnBattleFieldInfo
    {
        public string enemyName;
        public int id;
        public Vector3 position;
    }
    
    [System.Serializable]
    public class BattleFieldInfo
    {
        public string name = "BattleField";
        public float width = 4;
        public float height = 10;
        public Material material;

        public ObstacleInfo[] obstacles;
        public EnemyOnBattleFieldInfo[] enemies;
    }
    
    [CreateAssetMenu(fileName = "BattleFieldData", menuName = "Data/BattleFieldData", order = 0)]
    public class BattleFieldData : ScriptableObject
    {
        public BattleFieldInfo[] battleFieldInfo;
        
    }
}