using UnityEngine;

namespace Data
{
    [System.Serializable]
    public class WeaponInfo
    {
        public int damage = 1;
        public float fireRate = 1f;
        public GameObject bulletPrefab;
        public float bulletSpeed = 15f;
    }
    
    [CreateAssetMenu(fileName = "WeaponData", menuName = "Data/WeaponData", order = 0)]
    public class WeaponData : ScriptableObject
    {
        public WeaponInfo[] weaponInfo;
    }
}