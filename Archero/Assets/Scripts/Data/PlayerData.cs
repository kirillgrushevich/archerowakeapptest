using UnityEngine;

namespace Data
{
    [System.Serializable]
    public class PlayerInfo
    {
        public int health = 5;
        public float speed = 2f;
        public GameObject prefab;
    }
    
    [CreateAssetMenu(fileName = "PlayerData", menuName = "Data/PlayerData", order = 0)]
    public class PlayerData : ScriptableObject
    {
        public PlayerInfo[] playerInfo;
    }
}