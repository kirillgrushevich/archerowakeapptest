using System;
using UnityEngine;

namespace Data
{
    public class DataManager
    {
        private readonly GameData gameData;
        private readonly PlayerData playerData;
        private readonly EnemyData enemyData;
        private readonly BattleFieldData battleFieldData;
        private readonly WeaponData weaponData;
        
        public DataManager()
        {
            gameData = LoadData<GameData>();
            playerData = LoadData<PlayerData>();
            enemyData = LoadData<EnemyData>();
            battleFieldData = LoadData<BattleFieldData>();
            weaponData = LoadData<WeaponData>();
        }

        public BattleFieldInfo GetBattleFieldInfo(int id)
        {
            return battleFieldData.battleFieldInfo[id];
        }
        
        public PlayerInfo GetPlayerInfo(int id)
        {
            return playerData.playerInfo[id];
        }

        public EnemyInfo GetEnemyInfo(int id)
        {
            return enemyData.enemyInfo[id];
        }

        public WeaponInfo GetWeaponInfo(int id)
        {
            return weaponData.weaponInfo[id];
        }

        public GameObject GetCoinPrefab()
        {
            return gameData.coinPrefab;
        }

        private static T LoadData<T>() where T : ScriptableObject
        {
            return Resources.Load($"Data/{typeof(T).Name}") as T;
        }
    }
}