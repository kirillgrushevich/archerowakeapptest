using System;
using UnityEngine;

namespace Input
{
    public class InputKeyboardController : InputController
    {
        private void Update()
        {
            var horizontal = 0f;
            var vertical = 0f;
            
            if (UnityEngine.Input.GetKey(KeyCode.A))
            {
                horizontal -= 1f;
            }
            
            if (UnityEngine.Input.GetKey(KeyCode.D))
            {
                horizontal += 1f;
            }

            if (UnityEngine.Input.GetKey(KeyCode.W))
            {
                vertical += 1f;
            }
            
            if (UnityEngine.Input.GetKey(KeyCode.S))
            {
                vertical -= 1f;
            }
            
            OnMovementAction(horizontal, vertical); ;
        }
    }
}