﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Input
{
    public class InputManager
    {
        private float horizontalAxis;
        public float HorizontalAxis => horizontalAxis;

        private float verticalAxis;
        public float VerticalAxis => verticalAxis;

        private InputController inputController;

        public InputManager()
        {
#if UNITY_EDITOR
            GameObject obj = new GameObject("InputKeyboardController");
            inputController = obj.AddComponent<InputKeyboardController>();
#else

#endif
            inputController.MovementAction += OnMoveAction;

        }
        
        private void OnMoveAction(float horizontal, float vertical)
        {
            horizontalAxis = horizontal;
            verticalAxis = vertical;
        }
    }
}