using System;
using UnityEngine;

namespace Input
{
    public class InputController : MonoBehaviour
    {
        public event Action<float, float> MovementAction;

        protected void OnMovementAction(float horizontal, float vertical)
        {
            MovementAction?.Invoke(horizontal, vertical);
        }

    }
}