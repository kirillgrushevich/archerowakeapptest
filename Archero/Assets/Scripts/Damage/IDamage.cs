using UnityEngine;

namespace Damage
{
    public interface IDamage
    {
        int Damage { get; }

        void Setup(int damage);
        void SetDamage(Collider collider);
    }
}