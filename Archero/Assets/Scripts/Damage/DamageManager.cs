﻿using System.Collections.Generic;
using Enemies;
using Player;
using UnityEngine;

namespace Damage
{
    public class DamageManager
    {
        private readonly Dictionary<Collider, IHitBox> hitBoxes = new Dictionary<Collider, IHitBox>();
        private readonly List<EnemyController> enemies = new List<EnemyController>();
        private PlayerController player;

        public void AddEnemy(EnemyController enemyController)
        {
            enemies.Add(enemyController);
        }

        public void AddPlayer(PlayerController playerController)
        {
            player = playerController;
        }
        
        public void AddHitBox(Collider collider, IHitBox hitBox)
        {
            hitBoxes[collider] = hitBox;
        }

        public bool SetDamage(Collider hitCollider, int damage)
        {
            if (!hitBoxes.TryGetValue(hitCollider, out var hitBox))
            {
                return false;
            }

            hitBox.GetDamage(damage);
            return true;

        }
        
        public bool IsDamagePossible(Collider hitCollider)
        {
            return hitBoxes.ContainsKey(hitCollider);
        }

        public EnemyController TryToFindNearestEnemy(Vector3 position)
        {
            enemies.RemoveAll(e => e == null);
            
            EnemyController enemy = null;
            var distance = float.MaxValue;

            foreach (var controller in enemies)
            {
                var direction = controller.Head.position - position;
                var checkRayCast = CheckRayCast(position, direction);

                if (direction.sqrMagnitude > distance || !checkRayCast)
                {
                    continue;
                }
                
                enemy = controller;
                distance = direction.sqrMagnitude;
            }

            return enemy;
        }

        private bool CheckRayCast(Vector3 point, Vector3 direction)
        {
            if (!Physics.Raycast(point, direction, out var hit))
            {
                return false;
            }

            return hitBoxes.ContainsKey(hit.collider);
        }

    }
}
