using System;
using UnityEngine;

namespace Damage
{
    public interface IHitBox
    {
        int Health { get; set; }
        Transform Head { get; }
        Action DieAction { get; }
        void Register();
        void GetDamage(int damage);
    }
}