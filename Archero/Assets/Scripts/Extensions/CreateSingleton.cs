﻿using System;

namespace Extensions
{
    public class CreateSingleton<T> where T : class
    {
        private static T instance;
        public static T Instance
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }
                
                instance = (T)Activator.CreateInstance(typeof(T));
                return instance;
            }
        }
    }
}
