using System;
using UnityEngine;

namespace Movement
{
    public class MovementController : MonoBehaviour
    {
        [SerializeField] protected Transform root;

        protected float speed;
        protected Vector3 boundMin;
        protected Vector3 boundMax;
        
        
#if UNITY_EDITOR
        public virtual void Reset()
        {
            root = gameObject.transform;
        }
#endif

        public virtual void Setup(Vector3 center, float width, float height, float movementSpeed)
        {
            var halfWidth = width * 0.5f;
            var halfHeight = height * 0.5f;
            
            boundMin = new Vector3(center.x - halfWidth, center.y, center.z - halfHeight);
            boundMax = new Vector3(center.x + halfWidth, center.y, center.z + halfHeight);

            speed = movementSpeed;
        }

        public virtual void Move(Vector3 direction)
        {
            direction *= Time.deltaTime * speed;
            var nextPosition = root.position + direction;

            if (nextPosition.x < boundMin.x || nextPosition.x > boundMax.x)
            {
                nextPosition.x = root.position.x;
            }

            if (nextPosition.z < boundMin.z || nextPosition.z > boundMax.z)
            {
                nextPosition.z = root.position.z;
            }

            transform.position = nextPosition;
        }
    }
}