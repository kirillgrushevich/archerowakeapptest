using UnityEngine;

namespace Movement
{
    public class PlayerMovement : MovementController
    {
        private Rigidbody rig;

        public override void Setup(Vector3 center, float width, float height, float movementSpeed)
        {
            base.Setup(center, width, height, movementSpeed);
            rig = GetComponent<Rigidbody>();
        }

        public override void Move(Vector3 direction)
        {
            var velocity = speed * direction;
            var nextPosition = root.position + direction;

            if (nextPosition.x < boundMin.x || nextPosition.x > boundMax.x)
            {
                velocity.x = 0f;
            }

            if (nextPosition.z < boundMin.z || nextPosition.z > boundMax.z)
            {
                velocity.z = 0f;
            }

            rig.velocity = velocity;

            if (!(direction.sqrMagnitude > 0.1f))
            {
                return;
            }
            
            var targetRotation = Quaternion.LookRotation(direction);
            root.rotation = Quaternion.Lerp(root.rotation, targetRotation, Time.deltaTime * 10f);
        }
    }
}