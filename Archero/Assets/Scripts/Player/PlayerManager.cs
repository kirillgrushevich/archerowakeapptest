﻿using Core;
using Damage;
using Data;
using Movement;
using UnityEngine;
using Weapon;

namespace Player
{
    public class PlayerManager
    {
        private Game game;
        private MovementController playerMovement;
        private PlayerController player;

        public PlayerController PlayerController => player;

        public void SetupPlayer(PlayerInfo playerInfo, WeaponInfo weaponInfo)
        {
            game = Game.Instance;
        
            var obj = Object.Instantiate(playerInfo.prefab, Vector3.zero, Quaternion.identity);

            player = obj.GetComponent<PlayerController>();
            player.Setup(playerInfo, weaponInfo);
        }

    }
}
