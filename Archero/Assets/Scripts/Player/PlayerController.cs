using System;
using Core;
using Damage;
using Data;
using Movement;
using UnityEngine;
using Weapon;

namespace Player
{
    public class PlayerController : MonoBehaviour, IHitBox
    {
        [SerializeField] private Transform head;
        public Transform Head => head;
        
        private MovementController movementController;
        private WeaponController weaponController;
        private Game game;

        private float nextShotTime;
        
        public int Health { get; set; }
        public Action DieAction => OnPlayerDie;

        public void Register()
        {
            var colliderArray = transform.GetComponentsInChildren<Collider>();
            foreach (var hitCollider in colliderArray)
            {
                Game.Instance.Damage.AddHitBox(hitCollider, this);
            }
            
            Game.Instance.Damage.AddPlayer(this);
        }

        public void GetDamage(int damage)
        {
            Health -= damage;
            if (Health <= 0)
            {
                DieAction?.Invoke();
            }
        }

        public void Setup(PlayerInfo playerInfo, WeaponInfo weaponInfo)
        {
            game = Game.Instance;
            
            movementController = GetComponent<MovementController>();
            weaponController = GetComponent<WeaponController>();
            
            movementController.Setup(game.BattleField.Center, game.BattleField.Width, game.BattleField.Height, playerInfo.speed);
            weaponController.Setup(weaponInfo);

            Health = playerInfo.health;
        }

        private void Start()
        {
            Register();
        }

        private void OnPlayerDie()
        {
            Destroy(gameObject);
        }

        private void Update()
        {
            var direction = new Vector3(game.Input.HorizontalAxis, 0f, game.Input.VerticalAxis);
            movementController.Move(direction.normalized);

            if (direction.sqrMagnitude > 0.01f)
            {
                return;
            }
            
            var target = game.Damage.TryToFindNearestEnemy(head.position);
            if (!target)
            {
                return;
            }
            
            transform.LookAt(target.transform.position);
            weaponController.Shot();
        }
    }
}