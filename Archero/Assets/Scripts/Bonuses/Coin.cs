﻿using Core;
using Player;
using UnityEngine;

namespace Bonuses
{
    public class Coin : MonoBehaviour
    {
        private void Update()
        {
            transform.Rotate(45f * Time.deltaTime * Vector3.up);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.transform.root.GetComponent<PlayerController>())
            {
                return;
            }
            
            Game.Instance.Profile.AddCoin(1); 
            Destroy(gameObject);
        }
    }
}
