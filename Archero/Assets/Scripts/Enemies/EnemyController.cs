﻿using System;
using Core;
using Damage;
using UnityEngine;

namespace Enemies
{
    public class EnemyController : MonoBehaviour, IHitBox, IDamage
    {
        public int Health { get; set; }

        [SerializeField] private Transform head;
        [SerializeField] private int meleeDamage;
        public Transform Head => head;
        public Action DieAction => OnEnemyDie;

        public void Register()
        {
            var colliderArray = transform.GetComponentsInChildren<Collider>();
            foreach (var hitCollider in colliderArray)
            {
                Game.Instance.Damage.AddHitBox(hitCollider, this);
            }
            
            Game.Instance.Damage.AddEnemy(this);
        }

        public void GetDamage(int damage)
        {
            Health -= damage;
            if (Health <= 0)
            {
                DieAction?.Invoke();
            }
        }

        public int Damage => meleeDamage;
        public void Setup(int damage)
        {
            meleeDamage = damage;
        }

        public void SetDamage(Collider other)
        {
            if (other.isTrigger)
            {
                return;
            }
            
            var isPlayerHit = Game.Instance.Damage.SetDamage(other, Damage);

            //TODO TEST
            if (isPlayerHit)
            {
                Debug.Log($"The enemy {gameObject.name}  hit player melee {Damage} damage");
            }
        }

        private void Start()
        {
            Register();
        }

        private void OnTriggerEnter(Collider other)
        {
            SetDamage(other);
        }

        private void OnEnemyDie()
        {
            Destroy(gameObject);
            Game.Instance.BattleField.SetupCoin(head.position);
        }
    }
}
