using Core;
using Damage;
using Data;
using Movement;
using UnityEngine;
using Weapon;

namespace Enemies
{
    public class EnemyManager
    {
        public void AddEnemy(EnemyInfo enemyInfo, Vector3 position, WeaponInfo weaponInfo)
        {
            var obj = Object.Instantiate(enemyInfo.prefab);
            obj.transform.position = position;
            
            var meleeDamage = obj.GetComponent<IDamage>();
            meleeDamage.Setup(enemyInfo.meleeDamage);

            var health = obj.GetComponent<IHitBox>();
            health.Health = enemyInfo.health;

            var weapon = obj.GetComponent<WeaponController>();
            weapon.Setup(weaponInfo);

            var movement = obj.GetComponent<MovementController>();
            var battleField = Game.Instance.BattleField;
            movement.Setup(battleField.Center, battleField.Width, battleField.Height, enemyInfo.speed);
        }
    }
}