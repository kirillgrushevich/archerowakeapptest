using System.Collections;
using Core;
using UnityEngine;
using Weapon;

namespace Ai
{
    public class AiDistanceAttack : Node
    {
        [SerializeField] private float maxDistance;
        [SerializeField] private WeaponController weaponController;
        private Coroutine attackProcessCoroutine;

        public override NodeState Evaluate()
        {
            if (attackProcessCoroutine != null)
            {
                return NodeState.Running;
            }

            var player = Game.Instance.Player.PlayerController;
            if (!player)
            {
                return NodeState.Failure;
            }
            
            if ((transform.position - player.transform.position).sqrMagnitude > maxDistance * maxDistance)
            {
                return NodeState.Failure;
            }
            
            attackProcessCoroutine = StartCoroutine(AttackProcess());
            return NodeState.Success;
        }
        private IEnumerator AttackProcess()
        {
            weaponController.Shot();
            yield return new WaitForSeconds(1f);
            attackProcessCoroutine = null;
        }
    }
}