using Core;
using UnityEngine;

namespace Ai
{
    public class AiLookAtPlayer : Node
    {
        [SerializeField] private Transform root;
        public override NodeState Evaluate()
        {
            var player = Game.Instance.Player.PlayerController;
            if (!player)
            {
                return NodeState.Failure;
            }
            
            root.LookAt(player.transform.position);
            return NodeState.Running;
        }
    }
}