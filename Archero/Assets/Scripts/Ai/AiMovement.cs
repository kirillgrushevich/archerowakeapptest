using Core;
using Movement;
using UnityEngine;

namespace Ai
{
    public class AiMovement : Node
    {
        [SerializeField]
        private MovementController movementController;
        public override NodeState Evaluate()
        {
            var player = Game.Instance.Player.PlayerController;
            
            movementController.Move(player.transform.position - transform.position);
            return NodeState.Success;
        }
    }
}