using Core;
using Player;
using UnityEngine;

namespace Ai
{
    public class AiRadar : Node
    {
        [SerializeField] private Transform head;
        public override NodeState Evaluate()
        {
            var player = Game.Instance.Player.PlayerController;
            if (!player)
            {
                return NodeState.Failure;
            }
            
            return CheckRayCast(head.position, player.Head.position - head.position) ? NodeState.Running : NodeState.Failure;
        }
        
        private static bool CheckRayCast(Vector3 point, Vector3 direction)
        {
            return Physics.Raycast(point, direction, out var hit) ? hit.transform.root.GetComponent<PlayerController>() : false;
        }
    }
}