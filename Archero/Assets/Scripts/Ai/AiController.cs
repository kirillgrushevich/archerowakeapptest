using Ai;
using UnityEngine;

namespace Data
{
    public class AiController : MonoBehaviour
    {
        [SerializeField] private Node rootNode;

        public Node RootNode
        {
            get => rootNode;
            set => rootNode = value;
        }

        private void Update()
        {
            rootNode.Evaluate();
        }
    }
}