using System;
using Movement;
using UnityEngine;

namespace Ai
{
    public class AiPatrol : Node
    {
        private enum Direction
        {
            Horizontal,
            Vertical,
        }

        [SerializeField] private Transform root;
        [SerializeField] private MovementController movementController;
        [SerializeField] private Direction direction;
        [SerializeField] private float min = 3f;
        [SerializeField] private float max = 3f;

        private int movementDirection;
        private Vector3 startPosition;

        private void Start()
        {
            startPosition = root.position;
            movementDirection = -1;
        }

        public override NodeState Evaluate()
        {
            switch (direction)
            {
                case Direction.Horizontal:
                    movementController.Move(new Vector3(movementDirection, 0f, 0f));
                    CheckMovement();
                    break;
                
                case Direction.Vertical:
                    movementController.Move(new Vector3(0f, 0f, movementDirection));
                    CheckMovement();
                    break;
                default:
                    
                    throw new ArgumentOutOfRangeException();
            }
            
            return NodeState.Running;
        }

        private void CheckMovement()
        {
            switch (direction)
            {
                case Direction.Horizontal:
                    if (transform.position.x <= startPosition.x - min || transform.position.x >= startPosition.x + max)
                    {
                        movementDirection *= -1;
                    }
                    break;
                case Direction.Vertical:
                    if (transform.position.z <= startPosition.z - min || transform.position.z >= startPosition.z + max)
                    {
                        movementDirection *= -1;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}