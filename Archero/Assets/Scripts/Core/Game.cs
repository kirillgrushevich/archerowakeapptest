﻿using Extensions;
using Input;
using BattleField;
using Damage;
using Data;
using Enemies;
using Player;
using Ui;
using UnityEngine;

namespace Core
{
    public class Game : CreateSingleton<Game>
    {
        private InputManager inputManager;
        public InputManager Input => inputManager;
       
        private readonly BattleFieldManager battleField;
        public BattleFieldManager BattleField => battleField;

        private DamageManager damage;
        public DamageManager Damage => damage;

        private PlayerManager player;
        public PlayerManager Player => player;

        private EnemyManager enemy;
        public EnemyManager Enemy => enemy;

        private AiManager ai;
        public AiManager Ai => ai;
        
        private UiManager ui;
        public UiManager Ui => ui;

        private DataManager data;
        public DataManager Data => data;

        private ProfileManager profile;
        public ProfileManager Profile => profile;

        public Game()
        {
            profile = new ProfileManager();
            data = new DataManager();
            damage = new DamageManager();
            battleField = new BattleFieldManager();
            player = new PlayerManager();
            enemy = new EnemyManager();
            ai = new AiManager();
            inputManager = new InputManager();
            ui = new UiManager();
        }

        public void Init()
        {
            var battleFieldInfo = data.GetBattleFieldInfo(0);
            battleField.CreateBattleField(battleFieldInfo);

            var playerInfo = data.GetPlayerInfo(0);
            var weaponInfo = data.GetWeaponInfo(0);

            player.SetupPlayer(playerInfo, weaponInfo);

            weaponInfo = data.GetWeaponInfo(1);
            foreach (var enemyOnBattleField in battleFieldInfo.enemies)
            {
                var enemyInfo = data.GetEnemyInfo(enemyOnBattleField.id);
                enemy.AddEnemy(enemyInfo, enemyOnBattleField.position + battleField.Center, weaponInfo);
            }
        }

    }
}
