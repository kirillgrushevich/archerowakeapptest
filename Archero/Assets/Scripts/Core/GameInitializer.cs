using System;
using UnityEngine;

namespace Core
{
    public class GameInitializer : MonoBehaviour
    {
        private void Start()
        {
            Game.Instance.Init();
        }
    }
}