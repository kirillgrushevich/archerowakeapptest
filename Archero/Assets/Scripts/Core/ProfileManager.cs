using UnityEngine;

namespace Core
{
    public class ProfileManager
    {
        public ProfileManager()
        {
            coins = PlayerPrefs.GetInt("Coins", 0);
        }

        private int coins;

        public void AddCoin(int count)
        {
            coins += count;
            PlayerPrefs.SetInt("Coins", coins);
        }
    }
}