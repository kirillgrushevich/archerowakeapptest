using System;
using Core;
using Damage;
using UnityEngine;

namespace Weapon
{
    public class BulletController : MonoBehaviour, IDamage
    {
        [SerializeField] private int bulletDamage;
        [SerializeField] private Rigidbody rig;
        [SerializeField] private float speed;

        public void Setup(float bulletSpeed)
        {
            speed = bulletSpeed;
        }


        public int Damage
        {
            get => bulletDamage;
            set => bulletDamage = value;
        }

        public void Setup(int damage)
        {
            bulletDamage = damage;
        }

        public void SetDamage(Collider other)
        {
            if (other.isTrigger)
            {
                return;
            }

            Game.Instance.Damage.SetDamage(other, bulletDamage);
            Destroy(gameObject);
        }
        
        private void OnEnable()
        {
            rig.velocity = speed * transform.forward;
        }

        private void OnTriggerEnter(Collider other)
        {
            SetDamage(other);
        }
    }
}