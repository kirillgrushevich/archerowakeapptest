using System;
using Damage;
using Data;
using UnityEngine;

namespace Weapon
{
    public class WeaponController : MonoBehaviour
    {
        [SerializeField] private Transform firePoint;
        
        private float nextShotTime;
        private float fireRate;
        private BulletController bullet;

        public void Setup(WeaponInfo info)
        {
            fireRate = info.fireRate;
            var obj = Instantiate(info.bulletPrefab, firePoint.position, firePoint.rotation);
            obj.SetActive(false);
            bullet = obj.GetComponent<BulletController>();
            bullet.Setup(info.bulletSpeed);
            ((IDamage) bullet).Setup(info.damage);
        }
        
        public void Shot()
        {
            if (Time.time <= nextShotTime)
            {
                return;
            }
            
            //TODO move to OBJECTS POOL
            var newBullet = Instantiate(bullet, firePoint.position, firePoint.rotation);
            newBullet.gameObject.SetActive(true);
            nextShotTime = Time.time + fireRate;
        }
    }
}